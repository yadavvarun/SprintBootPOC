package com.hcl;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Car {
	private String name;
	private int modal;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getModal() {
		return modal;
	}
	public void setModal(int modal) {
		this.modal = modal;
	}

}
