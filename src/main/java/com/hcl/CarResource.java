package com.hcl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarResource 
{
	@RequestMapping("cars")
	public List<Car> getCars()
	{
		List<Car> cars=new ArrayList<>();
		
		Car c1= new Car();
		c1.setModal(2018);
		c1.setName("BMW");
		
		Car c2= new Car();
		c2.setModal(2017);
		c2.setName("HONDA");
		
		cars.add(c1);
		cars.add(c2);
		
		return cars;
	}
	

}
